import urllib2
import blockcypher
BTC = blockcypher
import pyConf


def GetVal():
    req = urllib2.Request(pyConf.apibase + "bitcoin.php?key=" + pyConf.apikey, None, {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    response = response.replace('<br>', '\n', 10)
    response = response.replace('&', '', 10)
    response = response.replace('#', '', 10)
    response = response.replace('pound;', '', 10)
    response = response.replace('euro;', '', 10)
    response = response.replace('36;', '', 10)
    return response

def GetBal(id):
    try:
      sbal=BTC.get_total_balance(id)
      bal=BTC.from_satoshis(sbal, 'btc')
      num=BTC.get_total_num_transactions(id)
      return ('Balance: '+str(bal)+'BTC\n Total Transactions: '+str(num))
    except:
      return 'Failed retrieving balance'