import hashlib


def hash(type, string):
    if type == 'md5':
        string = hashlib.md5(string).hexdigest()
        return string
    if type == 'sha256':
        string = hashlib.sha256(string).hexdigest()
        return string
    if type == 'sha224':
        string = hashlib.sha224(string).hexdigest()
        return string
    if type == 'sha1':
        string = hashlib.sha1(string).hexdigest()
        return string
    if type == 'sha512':
        string = hashlib.sha512(string).hexdigest()
        return string
    if type == 'sha384':
        string = hashlib.sha384(string).hexdigest()
        return string
