import urllib2

import pyConf

desc = " <skype> Attemp to resolve someone's IP from skype name"


def Res(ip):
    req = urllib2.Request('url' + ip, None, {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    return response


def Res2(ip):
    req1 = urllib2.Request(pyConf.apibase + "geoip.php?key=" + pyConf.apikey + "&ip=" + ip, None,
                           {'User-Agent': 'Mozilla/5.0'})
    response1 = urllib2.urlopen(req1).read()
    response1 = response1.replace('<br>', '\n', 20)
    return response1


def Res3(ip):
    req = urllib2.Request('url' + ip, None, {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    response = response.replace('<BR>', '\n', 20)
    return response
