from os import listdir
from os.path import isfile, join

import pyConf

onlyfiles = [f for f in listdir(pyConf.dirCommands) if isfile(join(pyConf.dirCommands, f))]
blist = ["Help.pyc", "__init__.py", "__init__.pyc", "Isup.pyc", "Resolve.pyc", 'Adfly.pyc', 'GeoIP.pyc', 'Host2Ip.pyc',
         'ISP.pyc', 'Joke.pyc', 'Joke.pyc', 'McServerInfo.pyc', 'Mode.pyc', 'Name2uuid.pyc', 'PortScanner.pyc',
         'Screen.pyc', 'Snap.pyc', 'Tuber.pyc', 'VBooter.pyc', 'YoMama.pyc', 'PortChecker.pyc', "Addadmin.pyc",
         'Admins.pyc', 'Banneds.pyc', 'Ban.pyc', 'Unban.pyc', 'RemoveAdmin.pyc', 'Encode.pyc', 'HashIdentifier.pyc',
         'Credits.pyc']
chelp = []
for v in onlyfiles:
    if v not in blist:
        chelp.append(v)

cleanhelp = [c.replace('.py', '   -   ') for c in chelp]
cleanhelp = '\n'.join(cleanhelp)
loaded = len(chelp)
h = open(pyConf.fileCommand, 'r')
help = h.read()
ah = open(pyConf.fileACmd, 'r')
ahelp = ah.read()
h.close
ah.close
