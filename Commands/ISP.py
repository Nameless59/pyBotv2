import urllib2


def isp(ip):
    req = urllib2.Request('url' + ip, None, {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    return response
