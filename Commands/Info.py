import os
import platform

import pyConf

string = (
'\nCoder: ' + pyConf.coder + ' \nBotName: ' + pyConf.name + '\nRunning OS: ' + os.name + '\nMachine: ' + platform.machine() + '\nPlatform: ' + platform.platform() + '\nDistro: ' + str(
    platform.dist()) + '\nRelease Version: ' + platform.release() + '\nSystem: ' + platform.system())
