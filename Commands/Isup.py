import urllib2

desc = " <url> Tell you if the website is online or offline"


def isUP(url):
    try:
        source = urllib2.urlopen('http://www.downforeveryoneorjustme.com/' + url).read()
        if source.find('It\'s just you.') != -1:
            return 'Site is Up'
        elif source.find('It\'s not just you!') != -1:
            return 'Site is Down'
        elif source.find('Huh?') != -1:
            return 'Invalid'
        else:
            return 'Unknown'
    except:
        return 'Unknown error'
