import urllib2

import pyConf


def joke():
    req = urllib2.Request(pyConf.apibase + "joke.php?key=" + pyConf.apikey, None, {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    return response
