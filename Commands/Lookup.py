import urllib2


def look(usr):
    req = urllib2.Request('url' + usr, None, {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    response = response.replace('Resp', 'No result found.', 1)
    return response
