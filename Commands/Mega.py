from mega import Mega

import pyConf
import pyFunctions

mega = Mega()
mega = Mega({'verbose': True})
m = mega.login()


def mHelp():
    h = open(pyConf.fileMegaH, 'r')
    help = h.read()
    return help


def mSpace():
    space = m.get_storage_space(mega=True)
    details = str(space)
    details = details.replace('}', '', )
    details = details.replace('{', '', )
    details = details.replace("'", '', 8)
    details = details.replace(", ", '\n', 1)
    details = details.replace("total", 'Total', 1)
    details = details.replace("L", 'Mb', 1)
    details = details.replace("used", 'Used', 1)
    return details


def mUpload(dir):
    file = m.upload(pyConf.dirMegaFiles + dir)
    tined = pyFunctions.adfLY(m.get_upload_link(file), convert=True)
    return tined


def mGetFiles():
    fs = str(m.get_files())
    return fs


def mCreateFold(name):
    m.create_folder(name)


def mDownload(name):
    file = m.find(name)
    link = m.get_link(file)
    tined = pyFunctions.adfLY(link, convert=True)
    return tined
