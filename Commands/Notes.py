import pyConf
import pyFunctions


def getnotes(user):
    user = pyFunctions.getcleanuser(user)
    try:
     n = open(pyConf.dirNotes + '\\' + user + '.txt', 'r')
     cont = n.read()
     n.close
    except:
        return('You must create a note before! Type !note <text> to create one.')
    return ('Notes for ' + user + ': \n' + cont)


def createnotes(user, txt):
    user = pyFunctions.getcleanuser(user)
    n = open(pyConf.dirNotes + '\\' + user + '.txt', 'w+')
    n.write(txt)
    n.close()
    return ('Written: \n"' + txt + '"\n On ' + user + ' note file.')


def editnote(user, txt):
    user = pyFunctions.getcleanuser(user)
    try:
     n = open(pyConf.dirNotes + '\\' + user + '.txt', 'r')
     cont = n.read()
     n.close
     n = open(pyConf.dirNotes + '\\' + user + '.txt', 'w')
     n.write(cont + '\n' + txt)
     n.close()
    except:
        return('You must create a note before! Type !note <text> to create one.')
    return ('Edited ' + user + "'s note.Type " + pyConf.trigger + 'getnote to get your note contenent.')
