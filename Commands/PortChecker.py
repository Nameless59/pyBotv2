import urllib2


def check(ip, port):
    req = urllib2.Request('apiurl' + ip + '&port=' + port, None,
                          {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    return response
