import pyConf


def remove(usr):
    pyConf.admins.remove(usr)
    alist = open('Admin/Alist.txt', 'r')
    lines = alist.readlines()
    alist.close()
    alist = open('Admin/Alist.txt', 'w')
    alist.write('\n'.join(pyConf.admins))
    alist.close()
