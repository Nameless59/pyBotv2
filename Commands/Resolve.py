#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib2

import pyConf

desc = " <skype> Attemp to resolve someone's IP from skype name"


def Res(usr):
    req = urllib2.Request(pyConf.apibase + "resolve.php?key=" + pyConf.apikey + "&server=1&name=" + usr, None,
                          {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    return response


def Res2(usr):
    req1 = urllib2.Request(pyConf.apibase + "resolve.php?key=" + pyConf.apikey + "&server=2&name=" + usr, None,
                           {'User-Agent': 'Mozilla/5.0'})
    response1 = urllib2.urlopen(req1).read()
    return response1


def Res3(usr):
    req2 = urllib2.Request(pyConf.apibase2+"&action=resolve&string="+usr, None,
                           {'User-Agent': 'Mozilla/5.0'})
    response2 = urllib2.urlopen(req2).read()
    response2.decode('utf-8')
    return response2


def Res4(usr):
    req3 = urllib2.Request('apiurl' + usr, None, {'User-Agent': 'Mozilla/5.0'})
    response3 = urllib2.urlopen(req3).read()
    response3 = response3.replace('Crap, No IP Was Found!', 'IP Not Found.', 1)
    return response3


def Res5(usr):
    req7 = urllib2.Request('apiurl' + usr, None,
                           {'User-Agent': 'Mozilla/5.0'})
    response7 = urllib2.urlopen(req7).read()
    response7 = response7.replace('\n', '', 1)
    return response7


def Res6(usr):
    req8 = urllib2.Request('apiurl' + usr, None,
                           {'User-Agent': 'Mozilla/5.0'})
    response8 = urllib2.urlopen(req8).read()
    return response8
