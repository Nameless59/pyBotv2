import urllib2

import pyFunctions


def Screen(url):
    req = urllib2.Request('apiurl' + url, None, {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    responsemod = pyFunctions.tinyURL(response)
    return responsemod
