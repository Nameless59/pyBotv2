import urllib2

import pyConf
import pyFunctions


def snap():
    req = urllib2.Request(pyConf.apibase + "snap.php?key=" + pyConf.apikey + '&output=URL', None,
                          {'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    response = pyFunctions.tinyURL(response)
    return response
