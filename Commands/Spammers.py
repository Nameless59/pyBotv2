import pyConf
import pyFunctions
import os
from os import listdir
from os.path import isfile, join

def getSpammers():
    i = 0
    t = []
    list = [f for f in listdir(pyConf.dirUsers) if isfile(join(pyConf.dirUsers, f))]
    spammers= []
    while i < len(list):
        user = list[i]
        user = user.replace('.txt','',1)
        usrmessages = pyFunctions.userMsg(user,False)
        spammers.append(str(usrmessages)+' :'+str(i)+':')
        i += 1
    spammers.sort(reverse=True)
    pos = int(pyFunctions.findBetween(spammers[0],':',':'))
    t.append(list[pos])
    t[0] = t[0].replace('.txt','',1)
    return t[0]