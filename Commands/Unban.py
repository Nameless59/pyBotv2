import pyConf


def unban(usr):
    pyConf.banned.remove(usr)
    alist = open(pyConf.fileBanned, 'r')
    lines = alist.readlines()
    alist.close()
    alist = open(pyConf.fileBanned, 'w')
    alist.write('\n'.join(pyConf.banned))
    alist.close()
