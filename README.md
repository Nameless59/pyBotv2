# pyB0t 2.2 #
 Based on pyBot 1.1.3 
# What is this repository for? #

This is a skypebot with:

* Chat commands
* Banning system
* Admin management
* Logging system

***

# How do I get set up? #

* Install [Python 2.*](https://www.python.org/download/releases/2.7/) or newer(i use 2.7.10)
* Download and install [Pip](https://pypi.python.org/packages/source/p/pip/pip-7.1.2.tar.gz#md5=3823d2343d9f3aaab21cf9c917710196)
* Download and extract this project
* Run 'Install.bat'
* When finished run 'Start.bat'
* pyBot will ask you the permissions to connect to Skype([Example](http://i.imgur.com/RnMX60X.png)
* Accept it
* Done!
# More: #

* [Skype4py Documentation](skype4py.sourceforge.net/doc/html/)

* Little Skype4py [tutorial](http://www.mchacks.altervista.org/showthread.php?tid=352) or [this tut](https://leakforums.net/thread-641974)

* Need help? Contact me on [skype](skype:live:nameless59_1?add)

* Take a look at the [changelogs](https://gitlab.com/Nameless59/pyBotv2/blob/master/CHANGELOG)

# Chat Commands #

Available commands:

* help:                    Shows all available commands.
* resolve<user>:           Attemp to get an IP from a skype user.
* joke:                    Return a random joke.
* snap:                    Return a random snapchat nude.
* yamamma:                 Return a random yamamma joke.
* isp<ip>:                 Return ISP from the IP.
* geoip<ip>:               Return some IP infos(Location, Provider...)
* tube<text>:              Tube something for you.
* encode<type><text>.      Encode your message to a specific hash(Available:sha1,sha256,sha224,sha512,sha384,md5)
* isup<text>:              Check if a website is online or offline
* bannedlist:              Return the list of banned users.
* adminlist:               Return the list of admin users.
* credits:                 Return the credits of the bot.
* info:                    Return some infos about who is running the bot.
* btcval:                  Retrieve current Bitcoin values.
* btc<address>             Return total <address> Bitcoins e Total transactions.
* imgur:                   Return a random Imgur image.
* giphy<tag>:              Get a random Giphy picture with specified tag.
* short<url>:              Return you url shortened(adfly).
* host2ip<host>:           Attempt to resolve the host ip.
* portscan<ip>:            Return a list of open ports in a specific IP.
* screensite<url>:         Take a screenshot of the given url and return you the screen url.
* boot<url>:               Send 16 request to a website(Buggy).
* serverinfo<url>:         Retrieve a list of informations about a Minecraft server.
* portcheck<ip><port>:     Check if a specific port is opened on a specific IP.
* pwgen <lenght>:          Generate a random password(Max 200 characters).
* buy:                     Buy a premium key for the bot.
* redeem<key>:             Redeem a premium key(Buy one here -> https://sellfy.com/Carb0n).

Commands for Premium users:

* mcalt:                   Generate a random Minecraft alt.
* imageiplogger:           Generate an image with an ip logger inside.
* mega <params>:           Mega.co.nz commands!Use mega help for mega command list.
* note <text>:             Create a note with <text>.
* getnote:                 Show your notes.
* editnote <text>:         Let you add something to your note.
* Premium users are also blacklisted from resolvers and other commands.

Commands for Admins of the bot:

* botmode<chat/owner>:  
* stop:                  Stop the bot.   
* spam:                  Spam shit.
* chatban:               Ban someone from the chat
* muteall<user>:         Mute everyone in the chat except of <user>.
* unmuteall:             Unmute everyone in the chat.
* kickall<user>:         Kick everyone from the chat except <user>.
* addadmin<user>:        Add admin to the bot.
* ban<user>:             Ban a user from the bot.
* unban<users:           Unban a user from the bot.
* removeadmin<user>:     Remove an admin from the bot.
* debug<true/false>:     Enabled/Disable chat debug.
* logtofile<true/false>: If true: Save the logs into logs file
                        If false: Just print the logs in the console.
* nick<nick>:            Set <nick> as new running user's displayname.
* mood<mood>:            Set <mood> as new running users' mood.
* chatinfo:              Grab some chat infos.
* addall:                Add all contacts to current chat.
* mass<msg>:             Send <msg> to all contacts.
* genkeys<number>:       Generate some premium keys and save them to the Keys file.
* savekeys:              Create a file for each key in Keys file with 1 key.
* addbots<qnt>:          Add <qnt> bots to the chat.
* kickbots:              Kick every user that contains 'bot' in the handle.
* stop:                  Stop the bot.
* reload:                Restart the bot.

Code is not 100% optimized, and maybe sometimes shitty, cause i started learning python recently...

***