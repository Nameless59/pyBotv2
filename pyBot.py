import pyDebug
import pyEvents
import pyFunctions
import pyPe
import os
import pyGraphic
os.system("title pyBot")
SKYPE = pyPe.getAPI()
# START
if __name__ == '__main__':
    pyDebug.info()
    pyDebug.action('Checking if all required packages are installed...')
    pyDebug.action('Needed modules: '+','.join(pyFunctions.checkPackages()))
    pyDebug.action('Loading Skype4Py API...')
    try:
        API = pyPe.getAPI()
        pyDebug.alert('Skype4Py API has been imported.')
    except:
        pyDebug.errorExit('Failed to import Skype4Py API! Quitting...')
    pyDebug.action('Checking for Skype Process...')
    if API.Client.IsRunning == True:
        pyDebug.alert('Skype process has been found.')
    else:
        pyDebug.error('Skype process not found! Starting...')
        try:
            API.Client.Start()
            pyDebug.alert('Skype process started.')
        except:
            pyDebug.errorExit('Failed to start Skype process! Quitting...')
    pyDebug.action('Checking for Skype version...')
    try:
        pyDebug.alert('You are using a compatible version of Skype.')
    except:
        pyDebug.errorExit('Failed to check Skype version. Quitting...')
    pyDebug.action('Connecting Skype4Py API...')
    try:
        API.OnAttachmentStatus = pyEvents.apiAttach
        API.Attach()
        API.OnMessageStatus = pyEvents.onMessage
        API.OnCallDtmfReceived = pyEvents.DTMF
    except:
        pyDebug.errorExit('Skype4Py API has failed to connect')
    pyDebug.action('Loading database...')
    try:
        pyFunctions.databaseLoad()
        pyDebug.alert('Database has been successfully loaded!')
    except:
        pyDebug.errorExit('Failed to load database! Quitting...')
    pyDebug.action('Loading Admin e Banned list...')
    try:
        pyFunctions.loadBanAdm()
        pyDebug.action('Admin e Banned list loaded!')
    except:
        pyDebug.error('Could not load Admin e Banned list! Ignoring...')
    pyDebug.action('Checking Directories...')
    try:
        pyFunctions.checkDirs()
    except:
        pyDebug.errorExit('Failed Loading/Creating Directories! Quitting...')
    pyDebug.action('Checking for updates...')
    pyFunctions.updateCheck()
    pyDebug.action('Logging in into Mega.co.nz...')
    try:
        pyFunctions.megaLogin()
        pyDebug.action('Successfully logged in...')
    except:
        pyDebug.error('Could not log in into mega! Ignoring...')
        SKYPE.CurrentUserProfile._SetRichMoodText('PyBot is online!Join the official chat!')

        raw_input('')
