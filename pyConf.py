import os
import pyFunctions

# dir location
cleanDir = os.getcwd()
cleanDir = cleanDir.strip()
dirCommands = os.getcwd() + '\Commands'
dirConfigs = os.getcwd() + '\Configs'
dirAdmin = os.getcwd() + '\Admin'
dirDox = os.getcwd() + '\Dox'
dirNotes = os.getcwd() + '\Notes'
dirLogs = os.getcwd() + '\Logs'
dirMega = os.getcwd() + '\Mega'
dirMegaFiles = dirMega + '\Files'
dirKeys = os.getcwdu() + '\Keys'
dirUsers = dirConfigs +'\Users Configs'

# files location
fileLogs = dirLogs + '\Logging.logs'
fileConfigs = dirConfigs+'\Config.txt'
fileAlist = dirAdmin + '\Alist.txt'
fileACmd = dirAdmin + '\AdmCmd.txt'
fileBanned = dirConfigs + '\Banned.txt'
fileCommand = dirConfigs + '\Commands.txt'
fileTos = dirConfigs + '\Tos.txt'
fileCredits = dirConfigs + '\Credits.txt'
fileFilter = dirConfigs + '\Filter.txt'
fileAds = dirConfigs + '\Ads.txt'
fileDbase = dirLogs + '\Database.txt'
fileMegaH = dirMega + '\MCommands.txt'
filePremium = dirConfigs + '\PUsers.txt'
fileKeys = dirKeys + '\Keys.txt'

# stufffile
allconfigs = []
admins = []
banned = []
pusers = []
adList = []
usrmessages = []
dbase = []
filterList = []
apikey = ''
apibase2 = ''
apibase = 'http://api.rooting.xyz/'
memail = ''
mpass = ''
premiumKeys = []

# info
coder = 'Carb0n'
name = 'pyBot'
version = '2.2'

# conf
adCount = 0
graphic = False
debug = ''
lastMSG = ''
lastHandle = ''
mode = ''
trigger = ''
logtofile = ''
language = ''
autokick = ''
filtermode = ''
