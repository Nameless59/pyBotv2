# -*- coding: latin-1 -*-
import sys
import pyConf
import pyFunctions
import os
i = 0
def logaction(msg, user):
    print '[#] - ' + msg
    user = pyFunctions.getcleanuser(user)
    if pyConf.logtofile == 'true':
       try:
        dir = pyConf.dirLogs + '\\' + user + '.txt'
        log = open(dir, 'r')
        conte = log.read()
        log.close
        log = open(dir, 'w+')
        log.write(conte + '\n' + msg)
        log.close
       except:
           print('[!]Failed updating logs of '+user)


def alert(msg):
    print '[+] - ' + msg
    if pyConf.logtofile == 'true':
       try:
        if pyConf.fileLogs not in os.listdir(pyConf.dirLogs):
            log = open(pyConf.fileLogs, 'w+')
        log = open(pyConf.fileLogs, 'r')
        conte = log.read()
        log.close
        log = open(pyConf.fileLogs, 'w+')
        log.write(conte + '\n' + '[#] - ' + msg)
        log.close
       except:
           print('[!]Failed writing logs')


def action(msg):
    print '[+] - ' + msg
    if pyConf.logtofile == 'true':
       try:
        if pyConf.fileLogs not in os.listdir(pyConf.dirLogs):
            log = open(pyConf.fileLogs, 'w+')
        log = open(pyConf.fileLogs, 'r')
        conte = log.read()
        log.close
        log = open(pyConf.fileLogs, 'w+')
        log.write(conte + '\n' + '[#] - ' + msg)
        log.close
       except:
           print('[!]Failed writing logs')


def error(msg):
    print '[!] - ' + msg
    if pyConf.logtofile == 'true':
       try:
        if pyConf.fileLogs not in os.listdir(pyConf.dirLogs):
            log = open(pyConf.fileLogs, 'w+')
        log = open(pyConf.fileLogs, 'r')
        conte = log.read()
        log.close
        log = open(pyConf.fileLogs, 'w+')
        log.write(conte + '\n' + '[#] - ' + msg)
        log.close
       except:
           print('[!]Failed writing logs')

def errorExit(msg):
    if pyConf.logtofile == 'true':
        log = open(pyConf.fileLogs, 'r')
        conte = log.read()
        log.close
        log = open(pyConf.fileLogs, 'w+')
        log.write(conte + '\n' + '[#] - ' + msg)
        log.close
    error(msg)
    raw_input('\nPress the <ENTER> key to EXIT...')
    sys.exit()



def quit():
    sys.exit()


def info():
    print'|========================================================|'
    print'|Name: '+pyConf.name+'.............................................|'
    print'|Version: ' + str(pyConf.version)+'............................................|'
    print'|Wrote by '+pyConf.coder+' in Python...............................|'
    print'|File:   pyBot.py........................................|'
    print'|........########..##....##.########...#######..########.|'
    print'|........##.....##..##..##..##.....##.##....###....##....|'
    print'|........##.....##...####...##.....##.##...#.##....##....|'
    print'|........########.....##....########..##..#..##....##....|'
    print'|........##...........##....##.....##.##.#...##....##....|'
    print'|........##...........##....##.....##.###....##....##....|'
    print'|........##...........##....########...#######.....##....|'
    print'|========================================================|'


def prompt(msg):
    return raw_input('[?] - ' + msg + ' : ')


# VERSION CHECK
def versionCheck():
    if sys.version_info < (2, 7, 0):
        errorExit('Requires Python version 2.7 or higher. Quitting...')
    else:
        pass
