import pyManager
import pyDebug


def apiAttach(status):
    if status == 3:  #apiAttachNotAvailable
        pyDebug.errorExit('Skype4Py API has failed to connect.')
    elif status == 1:  #apiAttachPendingAuthorization
        pyDebug.action('Skype4Py API is pending authorization...')
    elif status == 2:  #apiAttachRefused
        pyDebug.errorExit('Skype4Py API has failed to connect.')
    elif status == 0:  #apiAttachSuccess
        pyDebug.alert('Skype4Py API has been connected.')
    elif status == -1: #apiAttachUnknown
        pyDebug.errorExit('Skype4Py API has failed to connect')

def DTMF(call, code):
    pyDebug.action('DTMF CODE : ' + code)


def onMessage(msg, status):
    pyManager.handle(msg, status).start()

    
