# -*- coding: utf-8 -*-
import hashlib
import os
import random
import re
import urllib2
import pyConf
try:
 from mega import Mega
except:
 pyConf.megamode = False

import pip
import pyDebug

if pyConf.megamode == True:
 mega = Mega()
 mega = Mega({'verbose': True})

#MEGA
def megaLogin():
    # m = mega.login(pyConf.memail, pyConf.mpass)
    m = mega.login()

#CHECK NECESSARY PACKAGES
def checkPackages():
    needed = ['Skype4py','pip','os','random','mega','urllib2','re','hashlib','glob','blockcypher','datetime','time','threading']
    installed_packages = pip.get_installed_distributions()
    flat_installed_packages = [package.project_name for package in installed_packages]
    return list(set(needed)-set(flat_installed_packages))



#DIR CCHECKER
def checkDirs():
    # Check Dir pyConfs
    if not os.path.exists(pyConf.dirConfigs):
        pyDebug.action(pyConf.dirConfigs + " doesn't exist. Creating...")
        try:
            os.makedirs(pyConf.dirConfigs)
            pyDebug.action(pyConf.dirConfigs + " Created!")
        except:
            pyDebug.errorExit("Sorry couldn't create '" + pyConf.dirConfigs + '! Quitting...')
    else:
        pyDebug.action(pyConf.dirConfigs + " OK!")
    # Check Dir Logs
    if not os.path.exists(pyConf.dirLogs):
        pyDebug.action(pyConf.dirLogs + " doesn't exist. Creating...")
        try:
            os.makedirs(pyConf.dirLogs)
            pyDebug.action(pyConf.dirLogs + " Created!")
        except:
            pyDebug.errorExit("Sorry couldn't create '" + pyConf.dirLogs + '! Quitting...')
    else:
        pyDebug.action(pyConf.dirLogs + " OK!")
    # Check Dir Notes
    if not os.path.exists(pyConf.dirNotes):
        pyDebug.action(pyConf.dirNotes + " doesn't exist. Creating...")
        try:
            os.makedirs(pyConf.dirNotes)
            pyDebug.action(pyConf.dirNotes + " Created!")
        except:
            pyDebug.errorExit("Sorry couldn't create '" + pyConf.dirNotes + '! Quitting...')
    else:
        pyDebug.action(pyConf.dirNotes + " OK!")
    # Check Dir Dox
    if not os.path.exists(pyConf.dirDox):
        pyDebug.action(pyConf.dirDox + " doesn't exist. Creating...")
        try:
            os.makedirs(pyConf.dirDox)
            pyDebug.action(pyConf.dirDox + " Created!")
        except:
            pyDebug.errorExit("Sorry couldn't create '" + pyConf.dirDox + '! Quitting...')
    else:
        pyDebug.action(pyConf.dirDox + " OK!")
    # Check Dir Admin
    if not os.path.exists(pyConf.dirAdmin):
        pyDebug.action(pyConf.dirAdmin + " doesn't exist. Creating...")
        try:
            os.makedirs(pyConf.dirAdmin)
            pyDebug.action(pyConf.dirAdmin + " Created!")
        except:
            pyDebug.errorExit("Sorry couldn't create '" + pyConf.dirAdmin + '! Quitting...')
    else:
        pyDebug.action(pyConf.dirAdmin + " OK!")


# ADF.LY
def adfLY(url, convert=True):
    filtersList = ['boards.4chan.org', 'tinychat', 'imgur.com', 'youtu', 'facebook.com', 'gyazo.com', '.bmp', '.gif',
                   '.jpg', '.png']
    for item in filtersList:
        if item in url:
            convert = False
            break
        else:
            pass
    if convert == True:
        try:
            # Please use this sign-up referal [http://adf.ly/?id=4363464] if you are going to use your own api key, it can earn you/me extra money!
            return urllib2.urlopen(
                'http://api.adf.ly/api.php?key=00384da0cb72b552bac9b0a28ae9f375&uid=4363464&advert_type=int&domain=adf.ly&url=' + url).read()
        except:
            return False
    else:
        return False


# LOAD DATABASE
def databaseLoad():
    adsFile = open(pyConf.fileAds, 'r')
    for line in adsFile.readlines():
        ad = line.replace('\n', '', 1)
        pyConf.adList.append(ad)
    pyConf.adList.sort()
    adsFile.close()
    pyConf.filterList = [line.strip() for line in open(pyConf.fileFilter, 'r')]
    banneds = open(pyConf.fileBanned, 'r')
    banneds.close()
    admin = open(pyConf.fileAlist, 'r')
    admin.close()
    credits = open(pyConf.fileCredits, 'r')
    credits.close
    pyConf.premiumKeys = [line.strip() for line in open(pyConf.fileKeys, 'r')]
    pyConf.admins = [line.strip() for line in open(pyConf.fileAlist, 'r')]
    pyConf.banned = [line.strip() for line in open(pyConf.fileBanned, 'r')]
    pyConf.dbase = [line.strip() for line in open(pyConf.fileDbase, 'r')]
    pyConf.pusers = [line.strip() for line in open(pyConf.filePremium, 'r')]
    pyConf.allconfigs = [line.strip() for line in open(pyConf.fileConfigs, 'r')]
    loadConfigs()



#LOAD CONFIGS
def loadConfigs():
    pyConf.trigger = (findBetween(pyConf.allconfigs[1],":",":"))
    pyConf.mode = (findBetween(pyConf.allconfigs[2],":",":"))
    pyConf.logtofile = (findBetween(pyConf.allconfigs[3],":",":"))
    pyConf.language = (findBetween(pyConf.allconfigs[4],":",":"))
    pyConf.debug = (findBetween(pyConf.allconfigs[5],":",":"))
    pyConf.lastMSG = (findBetween(pyConf.allconfigs[6],":",":"))
    pyConf.lastHandle = (findBetween(pyConf.allconfigs[7],":",":"))
    pyConf.adCount = int((findBetween(pyConf.allconfigs[8],":",":")))
    pyConf.filtermode = (findBetween(pyConf.allconfigs[9],":",":"))
    pyConf.autokick = (findBetween(pyConf.allconfigs[10],":",":"))

# GET BETWEEN
def getBetween(source, start, stop):
    data = re.compile(start + '(.*?)' + stop).search(source)
    if data:
        found = data.group(1)
        found = str(found)
        return found.replace('\n', '')
    else:
        return False


# GET CLEAN URL
def getCleanURL(url):
    cleanURL = url.replace('https://', '', 1)
    cleanURL = cleanURL.replace('http://', '', 1)
    cleanURL = cleanURL.replace('www.', '', 1)
    return cleanURL


# GET RANDOM
def getRandom(length):
    charSet = 'abcdefghijklmnopqrstuvwxyz'
    randomStr = ''.join(random.sample(charSet, length))
    return randomStr
#RESTART
def restart():
    os.system("Restart.bat")

#STOP
def stop():

    os.system("Stop.bat")


# GET TITLE
def getTitle(url):
    try:
        source = urllib2.urlopen(url).read()
        title = getBetween(source, '<title>', '</title>')
        return title
    except:
        return False


# GET WINDOWS
def getOS():
    if os.name == 'nt':
        return True
    else:
        return False


# TINYURL
def tinyURL(url):
    return urllib2.urlopen('http://tinyurl.com/api-create.php?url=' + url).read()


# CHECK FOR UPDATES
def updateCheck():
    latest = urllib2.Request('http://aledroid.altervista.org/SkypeBot/pyBot.txt',None,{'User-Agent': 'Mozilla/5.0'})
    resp = urllib2.urlopen(latest).read()
    splitCurrent = pyConf.version.split('.')
    splitLatest = resp.split('.')
    if int(splitLatest[0]) == int(splitCurrent[0]):
        if int(splitCurrent[1]) == int(splitLatest[1]):
                pyDebug.alert('You are using the latest updated version of pyBot.')
        elif int(splitCurrent[1]) < int(splitLatest[1]):
            pyDebug.error('Update to version ' + resp + ' now!')
        else:
            pyDebug.alert('You are using the latest updated version of pyBot.')
    elif int(splitCurrent[0]) < int(splitLatest[0]):
        pyDebug.error('Update to version ' + resp+ ' now!')
    else:
        pyDebug.alert('You are using the latest updated version of pyBot.')

#YTINFO
def ytinfo(code):
    req = urllib2.Request('https://hgcommunity.net/api/ytstats.php?data=all&id=' + code, None,{'User-Agent': 'Mozilla/5.0'})
    response = urllib2.urlopen(req).read()
    response = response.replace('<BR>', '\n', 20)
    return response


#LOGFILE CREATOR
def createlogfile(user):
    user = getcleanuser(user)
    dir = pyConf.dirLogs + '\\' + user + '.txt'
    dbase = open(dir, 'w+')
    dbase.close

#CLEANHANDLE
def getcleanuser(user):
    if '\\' or ':' or '*' or '|' in user:
        user = user.replace('\\', '', 50)
        user = user.replace('*', '', 50)
        user = user.replace(':', '', 50)
        user = user.replace('|', '', 50)
    else:
        pass
    return user

#DBASE ADD
def dbaseadd(user):
    pyConf.dbase.append(user)
    dbase = open(pyConf.fileDbase, 'w')
    dbase.write('\n'.join(pyConf.dbase))
    dbase.close
    user = getcleanuser(user)
    config = open (pyConf.dirUsers+"\\"+user+'.txt','w+')
    config.write('Commands = :0: \n Trigger = :!:')
    config.close
    saveDbase()

#ADD KEY
def addPremium(user, key):
    if key in pyConf.premiumKeys and user not in pyConf.pusers:
        usrs = open(pyConf.filePremium, 'r')
        conte = usrs.read()
        usrs.close
        usrs = open(pyConf.filePremium, 'w')
        usrs.write(conte + '\n' + user)
        usrs.close
        databaseLoad()
        usedKeys(key)
        return ('Key valid!User(' + user + ') added to premiums!')
        pyDebug.alert('Added a new premium user!'+user+' with "'+key+'" key.')
    else:
        if user not in pyConf.pusers:
            return ('Invalid key, buy one here: https://sellfy.com/Carb0n')
        else:
            return (user + ' is already premium!')

#GENERATE KEYS
def genKeys(length, hash,chara):
    import string
    if chara == True:
        chars = string.letters
    else:
     chars = string.letters + string.digits + string.punctuation
    pwdSize = length
    rand = ''.join((random.choice(chars)) for x in range(pwdSize))
    if hash == True:
        rand = hashlib.sha256(rand).hexdigest()
    return rand

#SAVEKEYS
def saveKeys():
    i = 0
    times = len(pyConf.premiumKeys)

    while times > 0:
        towrite = pyConf.premiumKeys[i]
        times -= 1
        i += 1
        Key = open(pyConf.dirKeys + '\Key' + str(i) + '.txt', 'w+')
        Key.write(towrite)

#USED KEYS
def usedKeys(key):
    pyConf.premiumKeys.remove(key)
    keys = open(pyConf.fileKeys, 'r')
    lines = keys.readlines()
    keys.close()
    keys = open(pyConf.fileKeys, 'w')
    keys.write('\n'.join(pyConf.premiumKeys))
    keys.close()

#SET LASTHANDLE E LASTMSG
def setConfigs(usr,msg,mode):
    if mode == 1:
     pyConf.lastHandle = usr
     pyConf.lastMSG = msg
     pyConf.allconfigs[6] = 'LastMSG = :'+msg+':'
     pyConf.allconfigs[7] = 'LastHandle = :'+usr+':'
    elif mode == 2:
     pyConf.trigger = msg
     pyConf.allconfigs[1] = 'Trigger = :'+msg+':'
    elif mode == 3:
     pyConf.mode = msg
     pyConf.allconfigs[2] = 'Mode = :'+msg+':'
    elif mode == 4:
     pyConf.logtofile = msg
     pyConf.allconfigs[3] = 'Logtofile = :'+msg+':'
    elif mode == 5:
     pyConf.debug = msg
     pyConf.allconfigs[5] = 'Debug = :'+msg+':'
    elif mode == 6:
     pyConf.filtermode = msg
     pyConf.allconfigs[9] = 'Filter = :'+msg+':'
    elif mode == 7:
     pyConf.filtermode = msg
     pyConf.allconfigs[10] = 'Autokick = :'+msg+':'
    confs = open(pyConf.fileConfigs,'w')
    confs.write('\n'.join(pyConf.allconfigs))
#FINDBETWEEN(STRING)
def findBetween(s, first, last):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

#FILTER
def wordFilter(msg, status=False):
    if msg.startswith('https://') or msg.startswith('http://') or msg.startswith('www.'):
        return False
    else:
        for item in pyConf.filterList:
            split = item.split(':')
            word = split[0]
            new = split[1]
            if word in msg:
                msg = msg.replace(word, new)
                status = True
            else:
                pass
        if status == True:
            return msg
        else:
            return False


#USER MESSAGES SENT
def userMsg(usr,Modify = False):
   usr = getcleanuser(usr)
   userconfig = [line.strip() for line in open(pyConf.dirUsers+"\\"+usr+'.txt', 'r')]
   commands = int(findBetween(userconfig[0],':',':'))
   if Modify == True:
    commands +=1
   userconfig[0]='Commands = :'+str(commands)+':'
   cmds = open(pyConf.dirUsers+"\\" +usr+'.txt', 'w')
   cmds.write('\n'.join(userconfig))
   return str(commands)

#SAVE DATABASE
def saveDbase():
    dbase = open(pyConf.fileDbase, 'w')
    dbase.write('\n'.join(pyConf.dbase))
    dbase.close()
