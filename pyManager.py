#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import random
import threading
import time

import pyConf
import pyDebug
import pyFunctions
import pyPe
# TODO: Auto File Loader instead of this shit
from Commands import Addadmin
from Commands import Adfly
from Commands import Admins
from Commands import WebDav
from Commands import AltGen
from Commands import Ban
from Commands import Banneds
from Commands import Bitcoin
from Commands import Credits
from Commands import Encode
from Commands import GeoIP
from Commands import HashIdentifier
from Commands import Help
from Commands import Host2Ip
from Commands import ISP
from Commands import ImageLogger
from Commands import Info
from Commands import Isup
from Commands import Joke
from Commands import Lookup
from Commands import McServerInfo
from Commands import Mega
from Commands import Mode
from Commands import Notes
from Commands import PortChecker
from Commands import PortScanner
from Commands import RemoveAdmin
from Commands import Resolve
from Commands import Screen
from Commands import Snap
from Commands import Tuber
from Commands import Unban
from Commands import VBooter
from Commands import Weather
from Commands import YoMama
from Commands import Imgur
from Commands import Giphy
from Commands import Pastebin
from Commands import Phone
from Commands import Spammers

SKYPE = pyPe.getAPI()


def shutdown(self):
    del self.SKYPE


class handle(threading.Thread):
    def __init__(self, Message, Status):
        self.cmdList = []
        self.cmdMSG = ''
        self.Message = Message
        self.Status = Status
        self.nick = '[#]'
        threading.Thread.__init__(self)

    def run(self):
        chat = self.Message.Chat
        msg = self.Message.Body
        send = chat.SendMessage
        senderDisplay = self.Message.FromDisplayName
        senderHandle = self.Message.FromHandle
        filterStatus = pyFunctions.wordFilter(msg.lower())

            # WORLD FILDER
        if filterStatus != False and pyPe.getAdmin(chat) == True and senderHandle not in pyConf.admin:
                self.Message.Body = filterStatus
                
        #ASS CHECK
        if pyConf.mode == 'ass' and pyPe.getAdmin(chat) == True and self.Status == 'RECEIVED' and senderHandle not in pyConf.admins:
                        option  = random.choice([True, False, False,False])
                        if option == True:
                            self.Message.Body = msg + ' in my ass.'
                        else:
                            pass

        #USER COMMAND HANDLER
        if (self.Status == 'RECEIVED' or self.Status == 'SENT') and pyConf.mode != 'owner' and msg.startswith(
                pyConf.trigger) and senderHandle not in pyConf.banned or msg.startswith('https://') or msg.startswith(
                'http://') or msg.startswith('www.'):
            G = datetime.datetime.now().strftime('%d')
            N = datetime.datetime.now().strftime('%m')
            H = datetime.datetime.now().strftime('%H')
            M = datetime.datetime.now().strftime('%M')
            # CHAT TYPE
            if self.Status == 'RECEIVED' and senderHandle not in pyConf.admins and senderHandle == pyConf.lastHandle and msg == pyConf.lastMSG and pyPe.getAdmin(chat) == True:
             send(self.nick + senderHandle + ', you are being kicked for spamming!')
             send('/kick ' + senderHandle)
            else:
             pyFunctions.setConfigs(senderHandle,msg,1)
            #DATABASE CHECK
            if senderHandle not in pyConf.dbase:
                db = send(self.nick + 'New user detected! Adding ' + senderHandle + ' to database!.')
                pyDebug.action('New user detected! Adding ' + senderHandle + ' to database!.')
                try:
                    pyFunctions.createlogfile(senderHandle)
                    pyFunctions.dbaseadd(senderHandle)
                    db.Body = (self.nick + 'Added ' + senderHandle + ' to database successfully!')
                    pyDebug.action('Added ' + senderHandle + ' to database successfully!')
                except:
                    db.Body = (self.nick + 'Error while adding ' + senderHandle + ' to database!')
            cmd = msg + ' command executed by ' + senderHandle + ' at ' + H + ':' + M + ' of ' + G + '/' + N
            pyDebug.logaction(cmd, senderHandle)

            # DEBUG CHECK
            if pyConf.debug == 'true':
                send(self.nick + cmd)


            if msg.startswith(pyConf.trigger + 'help'):
                help = send('Grabbing Help...')
                help.Body = Help.help

            elif msg.startswith(pyConf.trigger + 'mega ') or msg == pyConf.trigger + 'mega':
                todo = msg.replace(pyConf.trigger + 'mega ', '', 1)
                if todo == 'help' or todo == '':
                    dt = send(self.nick + 'Grabbing Mega Help...')
                    try:
                        dt = send(self.nick + Mega.mHelp())
                    except:
                        dt.Body = 'Failed retrieving Mega help.'
                elif todo == 'getspace':
                    dt = send(self.nick + 'Grabbing Mega Space details...')
                    try:
                        dt.Body = (Mega.mSpace())
                    except:
                        dt.Body = 'Failed retrieving Mega Space details.'
                elif 'upload' in todo:
                    dt = send(self.nick + 'Uploading...')
                    dir = todo.replace('upload ', '', 1)
                    try:
                        dt.Body = (self.nick + "Here is your link: " + Mega.mUpload(dir))
                    except:
                        dt.Body = 'Failed uploading files or returning link.'
                elif todo == 'getfiles':
                    dt = send(self.nick + 'Grabbing files list...')

                    dt.Body = (Mega.mGetFiles())
                elif 'newfolder' in todo:
                    name = todo.replace('newfolder ', '', 1)
                    dt = send(self.nick + 'Creating ' + name + '...')
                    Mega.mCreateFold(name)
                    dt.Body = (self.nick + 'Folder created!')
                elif 'geturl' in todo:
                    name = todo.replace('geturl ', '', 1)
                    dt = send(self.nick + 'Grabbing ' + name + ' url...')
                    dt.Body = (self.nick + Mega.mDownload(name))


            elif msg.startswith(pyConf.trigger + 'boot '):
                url = msg.replace(pyConf.trigger + 'boot ', '', 1)
                bot = send(self.nick + 'Booting...')
                try:
                    VBooter.boot(url)
                    bot.Body = ('Booted with 16 request')
                except:
                    bot.Body = (self.nick + 'Unknown error occurred')

            elif msg.startswith(pyConf.trigger + 'encode ') or msg == '!encode':
                string = msg.replace(pyConf.trigger + 'encode ', '', 1)
                splitstring = string.split(' ')
                items = len(splitstring)
                if string == '' or items < 2:
                    enc = send(
                        self.nick + 'Please use !encode <type> <string> format!Available encodes: sha1,sha256,sha224,sha512,sha384,md5\nExample: !encode sha256 test')
                else:
                    type = splitstring[0]
                    string = splitstring[1]
                    enc = send(self.nick + 'Encoding "' + string + '" to ' + type + '...')
                    time.sleep(1.2)
                    try:
                        enc.Body = (self.nick + Encode.hash(type, string))
                    except:
                        enc.Body = (self.nick + 'Unknown error while encoding string')

            elif msg.startswith(pyConf.trigger + 'lookup '):
                string = msg.replace(pyConf.trigger + 'lookup ', '', 1)
                enc = send(self.nick + 'Grabbing past ' + string + "'s ips...")
                try:
                    enc.Body = (self.nick + Lookup.look(string))
                except:
                    enc.Body = (self.nick + 'Unknown error occurred')

            elif msg.startswith(pyConf.trigger + 'host2ip '):
                host = msg.replace(pyConf.trigger + 'host2ip ', '', 1)
                hostip = send(self.nick + 'Grabbing ' + host + "'s ip...")
                try:
                    hostip.Body = (self.nick + 'Grabbed ip: ' + Host2Ip.Getip(host))
                except:
                    hostip.Body = (self.nick + 'Unknown error occurred')

            elif msg.startswith(pyConf.trigger + 'info'):
                info = send(self.nick + 'Retrieving infos...')
                info.Body = self.nick + Info.string

            elif msg.startswith(pyConf.trigger + 'credits'):
                credits = send(self.nick + 'Grabbing credits...')
                credits.Body = self.nick + Credits.string

            elif msg.startswith(pyConf.trigger + 'tube '):
                arg = msg.replace(pyConf.trigger + 'tube ', '', 1)
                try:
                    tube = send(self.nick + 'Searching result(1) for: ' + arg)
                    tube.Body = (self.nick + 'First: ' + Tuber.find(arg))
                except:
                    tube.Body = (self.nick + 'Unknown error')

            elif msg.startswith(pyConf.trigger + 'hashidentify '):
                string = msg.replace(pyConf.trigger + 'hashidentify ', '', 1)
                try:
                    hash = send(self.nick + 'Identifing...')
                    hash.Body = self.nick + HashIdentifier.identify(string)
                except:
                    hash.Body = (self.nick + 'Unknown error')

            elif msg.startswith(pyConf.trigger + 'resolve '):
                usr = msg.replace(pyConf.trigger + 'resolve ', '', 1)
                if usr not in pyConf.admins or pyConf.pusers:

                    resolve = send(
                        self.nick + 'Grabbing ' + usr + "'s IP...\nAPI1: Resolving... \nAPI2: Waiting turn...\nAPI3: Waiting turn...\nAPI4: Waiting turn...\nAPI5: Waiting turn...\nAPI6: Waiting turn...")
                    S1 = Resolve.Res(usr)
                    resolve.Body = self.nick + 'Grabbing ' + usr + "'s IP...\nAPI1: " + S1 + " \nAPI2: Resolving...\nAPI3: Waiting turn...\nAPI4: Waiting turn...\nAPI5: Waiting turn...\nAPI6: Waiting turn..."
                    S2 = Resolve.Res2(usr)
                    resolve.Body = self.nick + 'Grabbing ' + usr + "'s IP...\nAPI1: " + S1 + " \nAPI2: " + S2 + "\nAPI3: Resolving...\nAPI4: Waiting turn...\nAPI5: Waiting turn...\nAPI6: Waiting turn..."
                    S3 = Resolve.Res3(usr)
                    resolve.Body = self.nick + 'Grabbing ' + usr + "'s IP...\nAPI1: " + S1 + " \nAPI2: " + S2 + "\nAPI3: " + S3 + "\nAPI4: Resolving...\nAPI5: Waiting turn...\nAPI6: Waiting turn..."
                    S4 = Resolve.Res5(usr)
                    resolve.Body = self.nick + "Grabbing " + usr + "'s IP...\nAPI1: " + S1 + " \nAPI2: " + S2 + "\nAPI3: " + S3 + "\nAPI4: " + S4 + "\nAPI5: Resolving...\nAPI6: Waiting turn..."
                    S5 = Resolve.Res5(usr)
                    resolve.Body = self.nick + "Grabbing " + usr + "'s IP...\nAPI1: " + S1 + " \nAPI2: " + S2 + "\nAPI3: " + S3 + "\nAPI4: " + S4 + "\nAPI5: " + S5 + "\nAPI6: Resolving..."
                    S6 = Resolve.Res6(usr)
                    resolve.Body = self.nick + "Done!\nAPI1: " + S1 + " \nAPI2: " + S2 + "\nAPI3: " + S3 + "\nAPI4: " + S4 + "\nAPI5: " + S5 + "\nAPI6: " + S6
                else:
                    send(self.nick + 'User blacklisted')

            elif msg == (pyConf.trigger + 'btcval'):
                val = send(self.nick + 'Getting BTC current value...')
                val.Body = (self.nick + Bitcoin.GetVal())
            elif msg.startswith(pyConf.trigger + 'isup '):
                url = msg.replace(pyConf.trigger + 'isup ', '', 1)
                url = pyFunctions.getCleanURL(url)
                isup = send(self.nick + 'Checking website...')
                try:
                    isup.Body = (self.nick + Isup.isUP(url))
                except:
                    isup.Body = (self.nick + 'Unknown error occurred')
            elif msg.startswith(pyConf.trigger + 'weather '):
                city = msg.replace(pyConf.trigger + 'weather ', '', 1)
                weat = send(self.nick + 'Retrieving weather infos...')
                try:
                    weat.Body = (self.nick + Weather.GetWeath(city))
                except:
                    weat.Body = (self.nick + 'Unknown error occurred')
            elif msg.startswith(pyConf.trigger + 'pwgen '):
                length = msg.replace(pyConf.trigger + 'pwgen ', '', 1)
                if int(length) < 200:
                    pasw = send(self.nick + 'Generating ' + length + ' characters lenght password...')
                    try:
                        pasw.Body = (self.nick + 'Password: ' + pyFunctions.genKeys(int(length), hash=False,chara=False))
                    except:
                        pasw.Body = self.nick + 'Unknown error.'
                else:
                    send(self.nick + 'Password length must be under 200')
            elif msg.startswith(pyConf.trigger + 'screensite '):
                url = msg.replace(pyConf.trigger + 'screensite ', '', 1)

                isup = send(self.nick + 'Taking screenshot...')
                try:
                    isup.Body = (self.nick + ' ' + Screen.Screen(url))
                except:
                    isup.Body = (self.nick + 'Unknown error occurred')

            elif msg.startswith(pyConf.trigger + 'short '):
                short = send(self.nick + 'Shortening your url with Adfly...')
                url = msg.replace(pyConf.trigger + 'short ', '', 1)
                short.Body = self.nick + 'Shortened link: ' + Adfly.short(url)

            elif msg.startswith(pyConf.trigger + 'mass '):
                message = msg.replace(pyConf.trigger + 'mass ', '', 1)
                for friend in SKYPE.Friends:
                    if friend.OnlineStatus != 'OFFLINE':
                        try:
                            SKYPE.CreateChatWith(friend.Handle).SendMessage(self.nick + message)
                        except:
                            pass

            elif msg.startswith(pyConf.trigger + 'bannedlist'):
                bans = send(self.nick + 'Grabbing banned list...')
                try:
                    bans.Body = (self.nick + Banneds.list())
                except:
                    bans.Body = (self.nick + 'Failed retrieving banned list.')

            elif msg.startswith(pyConf.trigger + 'adminlist'):
                admins = send(self.nick + 'Grabbing admin list...')
                try:
                    admins.Body = (self.nick + Admins.list())
                except:
                    admins.Body = (self.nick + 'Failed retrieving admin list.')

            elif msg.startswith(pyConf.trigger + 'yamamma'):
                mama = send(self.nick + 'Grabbing yamamma...')
                try:
                    mama.Body = (self.nick + YoMama.mamma())
                except:
                    mama.Body = (self.nick + 'Failed retrieving yamamma.')

            elif msg.startswith(pyConf.trigger + 'snap'):
                mama = send(self.nick + 'Grabbing snap...')
                try:
                    mama.Body = (self.nick + ' ' + Snap.snap())
                except:
                    mama.Body = (self.nick + 'Failed retrieving snap.')

            elif msg.startswith(pyConf.trigger + 'joke'):
                joke = send(self.nick + 'Grabbing joke...')
                try:
                    joke.Body = (self.nick + Joke.joke())
                except:
                    joke.Body = (self.nick + 'Failed retrieving joke.')

            elif msg.startswith(pyConf.trigger + 'serverinfo '):
                ip = msg.replace(pyConf.trigger + 'serverinfo ', '', 1)
                joke = send(self.nick + 'Grabbing infos...')
                try:
                    joke.Body = (self.nick + McServerInfo.sinfo(ip))
                except:
                    joke.Body = (self.nick + 'Failed retrieving infos.')

            elif msg.startswith(pyConf.trigger + 'portscan '):
                ports = send(self.nick + 'Grabbing Open ports...')
                ip = msg.replace(pyConf.trigger + 'portscan ', '', 1)
                try:
                    ports.Body = (self.nick + PortScanner.scan(ip))
                except:
                    ports.Body = (self.nick + 'Failed retrieving list of open ports(maybe API offline).')

            elif msg.startswith(pyConf.trigger + 'redeem '):
                key = msg.replace(pyConf.trigger + 'redeem ', '', 1)
                val = send(self.nick + 'Checking key...')
                val.Body = (self.nick + pyFunctions.addPremium(senderHandle, key))

            elif msg.startswith(pyConf.trigger + 'webdav ') or msg.startswith(pyConf.trigger+'WebDAV '):
                url = msg.replace(pyConf.trigger + 'webdav ', '', 1)
                val = send(self.nick + 'Checking if website is vulnerable...')
                val.Body = (self.nick + WebDav.check(url))

            elif msg.startswith(pyConf.trigger + 'portcheck '):
                ports = send(self.nick + 'Checking port...')
                tosplit = msg.replace(pyConf.trigger + 'portcheck ', '', 1)
                splitted = tosplit.split(' ')
                ip = splitted[0]
                port = splitted[1]
                try:
                    ports.Body = (self.nick + PortChecker.check(ip, port))
                except:
                    ports.Body = (self.nick + 'Failed retrieving results(maybe API offline).')

            elif msg.startswith(pyConf.trigger + 'geoip '):
                ip = msg.replace(pyConf.trigger + 'geoip ', '', 1)
                geo = send(self.nick + 'Grabbing ' + ip + "'s Infos...\n API1: Resolving...")
                try:
                    S1 = GeoIP.Res(ip)
                    S1 = S1.replace('<br>', '\n', 11)
                    geo.Body = self.nick + 'Done' + ip + "'s Infos...\nAPI1:\n " + S1
                except:
                    geo.Body = self.nick + 'Unknown error occurred'

            elif msg.startswith(pyConf.trigger + 'isp '):
                ip = msg.replace(pyConf.trigger + 'isp ', '', 1)
                geo = send(self.nick + 'Grabbing ' + ip + "'s Infos...")
                try:
                    S1 = ISP.isp(ip)
                    geo.Body = self.nick + 'Grabbing ' + ip + "'s Infos...\n API1: " + S1
                except:
                    geo.Body = self.nick + 'Unknown error occurred'
                    
            elif msg.startswith(pyConf.trigger + 'btc '):
                id = msg.replace(pyConf.trigger + 'btc ', '', 1)
                bal = send(self.nick + 'Retrieving infos...')
                bal.Body = self.nick + Bitcoin.GetBal(id)

            elif msg.startswith(pyConf.trigger + 'giphy '):
                id = msg.replace(pyConf.trigger + 'giphy ', '', 1)
                bal = send(self.nick + 'Retrieving gif...')
                bal.Body = self.nick + Giphy.Find(id)

            elif msg == pyConf.trigger + 'me':
                info = send(self.nick + 'Retrieving infos...')
                try:
                    if senderHandle in pyConf.pusers and pyConf.admins:
                        status = 'Premium & Admin'
                    elif senderHandle in pyConf.admins:
                        status = 'Admin'
                    elif senderHandle in pyConf.pusers:
                        status = 'Premium'
                    else:
                        status = 'User'
                    info.Body = (self.nick + 'Handle: ' + senderHandle + '\nStatus: ' + status+'\nCommands executed: '+pyFunctions.userMsg(senderHandle,False)+'\nIP: Attempting to resolve...')
                    IP = Resolve.Res3(senderHandle)
                    info.Body = (self.nick + 'Handle: ' + senderHandle + '\nStatus: ' + status +'\nCommands executed: '+pyFunctions.userMsg(senderHandle,False)+'\nIP: '+IP)
                except:
                    info.Body = (self.nick + 'Unknown error.')
                    

            elif msg == pyConf.trigger+'users':
                usrs = send(self.nick+'Grabbing bot users')
                usrs.Body = (self.nick+'Total users: '+str(len(pyConf.dbase)))

            elif msg == pyConf.trigger + 'imgur':
                img = send(self.nick + 'Retrieving image.')

                img.Body = (self.nick + 'Image: ' + str(Imgur.get()))
            elif msg == pyConf.trigger + 'wkp':
                perc = 0
                string = 'Progress: [ . . . . . . . . . . . . . . . . . . . .]'
                stat = send(self.nick+string)
                done = False
                while done == False:
                    time.sleep(1)
                    perc +=5
                    string = string.replace(' .','#',1)
                    stat.Body = (self.nick+string)
                    
            elif msg.startswith(pyConf.trigger+'md5crack '):
                string = msg.replace(pyConf.trigger+'md5crack ','',1)
                crack = send(self.nick+'Trying to crack "'+string+'"')

                crack.Body = (self.nick+'Cracked string: '+Md5crack.crack(str(string)))
                
            elif msg.startswith(pyConf.trigger+'pastebin '):
                string = msg.replace(pyConf.trigger+'pastebin ','',1)
                paste =send(self.nick+'Uploading "'+string+'" to pastebin...')
                try:
                 paste.Body = (self.nick+'Your paste: '+Pastebin.upload(str(string)))
                except:
                   paste.Body = (self.nick+'Failed uploading paste.')


            elif msg.startswith(pyConf.trigger+'phone '):
                string = msg.replace(pyConf.trigger+'phone ','',1)
                phone =send(self.nick+'Retrieving "'+string+'" infos...')

                phone.Body = (self.nick+'Infos: '+Phone.check(str(string)))

            elif msg.startswith(pyConf.trigger+'spammers'):
                spam = Spammers.getSpammers()
                spammers = send(self.nick+'The top spammer is '+spam +' with '+pyFunctions.userMsg(spam,False)+' messages.')



        # URL CHECKER
            elif msg.startswith('https://') or msg.startswith('http://') or msg.startswith('www.'):
                url = pyFunctions.getCleanURL(msg)
                if 'youtube.com' in url or 'youtu.be' in url:
                    if 'youtu.be' in url:
                     code = url.replace('youtu.be/', '', 1)
                    else:
                     code = url.replace('youtube.com/watch?v=', '', 1)
                    infos = send(self.nick + 'Getting infos...')
                    try:
                     infos.Body = (self.nick + pyFunctions.ytinfo(code))
                    except:
                        infos.Body = (self.nick+'Failed retrieving infos.')
                else:
                    url = pyFunctions.getCleanURL(msg)
                    adflyCheck = pyFunctions.adfLY(url)
                    if adflyCheck != False and pyPe.getAdmin(chat) == True:
                        pyDebug.action('Detected a link...Shortening...')
                        try:
                            send(self.nick + ' ' + adflyCheck)
                        except:
                            pass
                    else:
                        pass

            if pyConf.adCount == 100:
                time.sleep(5)
                send(random.choice(pyConf.adList))
                pyConf.adCount = 0
                pyDebug.action('Sending Ad...')
            else:
                pyConf.adCount += 1
        else:
            pass

        # PREMIUM
        if senderHandle in pyConf.pusers or senderHandle in pyConf.admins and (self.Status == 'RECEIVED' or self.Status == 'SENT') and pyConf.mode != 'owner' and msg.startswith(
                pyConf.trigger) and senderHandle not in pyConf.banned:
            if msg.startswith(pyConf.trigger + 'imageiplogger'):
                img = send(self.nick + 'Generating ip logger...')
                img.Body = (self.nick + ImageLogger.gen())
            elif msg == (pyConf.trigger + 'mcalt'):
                alt = send(self.nick + 'Grabbing alt...')
                alt.Body = self.nick + 'Alt: ' + AltGen.gen()
            elif msg.startswith(pyConf.trigger + 'note '):
                note = send(self.nick + 'Writing new note for ' + senderHandle)
                text = msg.replace(pyConf.trigger + 'note ', '', 1)
                note.Body = (self.nick + Notes.createnotes(senderHandle, text))
            elif msg.startswith(pyConf.trigger + 'editnote '):
                note = send(self.nick + 'Editing note for ' + senderHandle)
                text = msg.replace(pyConf.trigger + 'editnote ', '', 1)
                note.Body = (self.nick + Notes.editnote(senderHandle, text))
            elif msg == (pyConf.trigger + 'getnote'):
                note = send(self.nick + 'Obtaining note for ' + senderHandle)
                note.Body = (self.nick + Notes.getnotes(senderHandle))
        else:
            pass
        # ONLY ADMINS
        if senderHandle in pyConf.admins and (self.Status == 'RECEIVED' or self.Status == 'SENT') and msg.startswith(pyConf.trigger) and senderHandle not in pyConf.banned:
            #BAN
            if msg.startswith(pyConf.trigger + 'ban '):
                usr = msg.replace(pyConf.trigger + 'ban ', '', 1)
                ban = send(self.nick + 'Banning ' + usr + ' from the bot.')
                try:
                    Ban.ban(usr)
                    ban.Body = (self.nick + 'Succesfully banned ' + usr + ' from the bot!')
                except:
                    ban.Body = (self.nick + "Unknown error occurred")
            #ADMINHELP
            elif msg == pyConf.trigger + 'admin help':
                send(Help.ahelp)
            #SPAM
            elif msg.startswith(pyConf.trigger + 'spam '):
                times = msg.replace(pyConf.trigger + 'spam ', '', 1)
                did = 0
                while did <= times:
                    send("skyp4py spam ftw")
                    did = did + 1
            #LISTCOMMANDS
            elif msg == (pyConf.trigger + 'lc'):
                send(Help.cleanhelp)
            #BOTMODE
            elif msg.startswith(pyConf.trigger + 'botmode '):
                modality = msg.replace(pyConf.trigger + 'botmode ', '', 1)
                mode = send(self.nick + 'Changing mode to ' + modality + '...')
                try:
                    pyFunctions.setConfigs(senderHandle,modality,3)
                    mode.Body = (self.nick + 'Current bot mode set to ' + modality)
                except:
                    mode.Body = self.nick + 'Unknown error.'
            #UNBAN
            elif msg.startswith(pyConf.trigger + 'unban '):
                usr = msg.replace(pyConf.trigger + 'unban ', '', 1)
                ban = send(self.nick + 'Unbanning ' + usr + ' from the bot.')
                if usr in pyConf.banned:
                    try:
                        Unban.unban(usr)
                        ban.Body = (self.nick + 'Succesfully unbanned ' + usr + ' from the bot!')
                    except:
                        ban.Body = (self.nick + "Unknown error occurred")
                else:
                    ban.Body = (self.nick + 'User is not banned!')
            #TRIGGER
            elif msg.startswith(pyConf.trigger + 'trigger '):
                trig = msg.replace(pyConf.trigger + 'trigger ', '', 1)
                pyFunctions.setConfigs(senderHandle,trig,2)
                send(self.nick + 'Trigger set to ' + trig)
            #ADDADMIN
            elif msg.startswith(pyConf.trigger + 'addadmin '):
                admin = send(self.nick + 'Adding...')
                usr = msg.replace(pyConf.trigger + 'addadmin ', '', 1)
                try:
                    Addadmin.add(usr)
                    admin.Body = (self.nick + 'Successfully added ' + usr + ' as new admin :)')
                except:
                    pyDebug.action("Failed adding a new admin...")
            #REMOVEADMIN
            elif msg.startswith(pyConf.trigger + 'removeadmin '):
                admin = send(self.nick + 'Removing...')
                usr = msg.replace(pyConf.trigger + 'removeadmin ', '', 1)
                if usr in pyConf.admins:
                    try:
                        RemoveAdmin.remove(usr)
                        admin.Body = (self.nick + 'Successfully removed ' + usr + ' from admin list :)')
                    except:
                        pyDebug.action("Failed removing an admin...")
                else:
                    admin.Body = (self.nick + 'User is not an admin')
            #ADDALL
            elif msg.startswith(pyConf.trigger + 'addall'):
                if self.Message.Chat.Type == 'MULTICHAT':
                    send('/topic Everyone')
                    try:
                        for friend in SKYPE.Friends:
                            if friend.OnlineStatus != 'OFFLINE':
                                try:
                                    send('/add ' + friend.Handle)
                                except:
                                    pass
                    except:
                        send(self.nick + "Sorry couldn't load contacts to add.")
                else:
                    send(self.nick + 'Must be on a group chat!')
            #MUTEALL
            elif msg.startswith(pyConf.trigger + 'muteall '):
                chatMembers = chat.MemberObjects
                if pyPe.getAdmin(chat) == True:
                    user = msg.replace(pyConf.trigger + 'muteall ', '', 1)
                    status = False
                    mutes = send(self.nick + 'Muting everyone...')
                    for name in chatMembers:
                        if name.Handle == user:
                            status = True
                        else:
                            pass
                    if status == True:
                        for name in chatMembers:
                            if name.Handle != senderHandle and name.Handle != user and name.Role != 'LISTENER':
                                try:
                                    send('/setrole ' + name.Handle + ' LISTENER')
                                    mutes.Body = (self.nick + 'Chat Unmuted.')
                                except:
                                    pass
                            else:
                                pass
                    else:
                        send(self.nick + user + ' is not found in this chat!')
                else:
                    send(self.nick + 'muteall requires chat admin.')
                    pyDebug.error(pyConf.trigger + 'muteall requires chat admin.')
            #UNMUTEALL
            elif msg.startswith(pyConf.trigger + 'unmuteall'):
                if pyPe.getAdmin(chat) == True:
                    chatMembers = chat.MemberObjects
                    mutes = send(self.nick + 'Unmuting everyone...')
                    for name in chatMembers:
                        if name.Handle != senderHandle and name.Role == 'LISTENER':
                            try:
                                send('/setrole ' + name.Handle + ' USER')
                                mutes.Body = (self.nick + 'Chat Unmuted.')
                            except:
                                pass
                else:
                    pyDebug.error('!unmuteall requires chat admin.')
            #KICKALL
            elif msg.startswith(pyConf.trigger + 'kickall '):
                if pyPe.getAdmin(chat) == True:
                    user = msg.replace(pyConf.trigger + 'kickall ', '', 1)
                    status = False
                    chatMembers = chat.MemberObjects
                    for name in chatMembers:
                        if name.Handle != user:
                            status = True
                        else:
                            pass
                    if status == True:
                        for name in chatMembers:
                            if name.Handle != senderHandle and name.Handle != user:
                                send('/kick ' + name.Handle)

                            else:
                                pass
                    else:
                        send(self.nick + user + ' is not found in this chat!')
                else:
                    pyDebug.error(pyConf.trigger + 'kickall requires chat admin.')
            #CHATBAN
            elif msg.startswith(pyConf.trigger + 'chatban '):
                if pyPe.getAdmin(chat) == True:
                    chatMembers = chat.MemberObjects
                    user = msg.replace(pyConf.trigger + 'chatban ', '', 1)
                    status = False
                    for name in chatMembers:
                        if name.Handle == user:
                            status = True
                            break
                        else:
                            pass
                    if status == True:
                        send('/kickban ' + user)
                    else:
                        send(self.nick + user + ' is not found in this chat!')
                else:
                    pyDebug.error(pyConf.trigger + 'chatban requires chat admin.')
            #NICK
            elif msg.startswith(pyConf.trigger + 'nick '):
                nick = msg.replace(pyConf.trigger + 'nick ', '', 1)
                stat = send(self.nick + 'Trying to change nick to "' + nick + '".')
                try:
                    SKYPE.CurrentUserProfile._SetFullName(nick)
                    stat.Body = (self.nick + 'Nick succesfully changed.')
                except:
                    stat.Body = (self.nick + 'Failed changing nick.(Maybe strange characters?')
            #MOOD
            elif msg.startswith(pyConf.trigger + 'mood '):
                mood = msg.replace(pyConf.trigger + 'mood ', '', 1)
                stat = send(self.nick + 'Trying to change mood into "' + mood + '".')
                try:
                    SKYPE.CurrentUserProfile._SetRichMoodText(mood)
                    stat.Body = (self.nick + 'Mood succesfully changed.')
                except:
                    stat.Body = (self.nick + 'Failed changing mood.(Maybe strange characters?')
            #DEBUG
            elif msg.startswith(pyConf.trigger + 'debug '):
                mode = msg.replace(pyConf.trigger + 'debug ', '', 1)
                md = send(self.nick + 'Setting debug mode to: ' + mode)
                try:
                    pyFunctions.setConfigs(senderHandle,mode,5)
                    md.Body = (self.nick + 'Debug mode set to ' + mode)
                except:
                    md.Body = (self.nick + 'Failed to set debug mode')
            #CHATINFO
            elif msg == (pyConf.trigger + 'chatinfo'):
                act_members = chat.ActiveMembers
                blob = chat.Blob
                members = chat.Members
                messages = chat.Messages
                role = chat.MyRole
                name = chat.Name
                recent_messages = chat.RecentMessages
                topic = chat.Topic
                send(
                    self.nick + 'Your role: ' + role + '\n Chat topic: ' + topic + '\n Chat Blob: ' + blob + '\n Chat Name:' + name)
            #CALLSPAM (TOFIX)
            elif msg.startswith(pyConf.trigger + 'callspam '):
                times = msg.replace(pyConf.trigger + 'callspam ', '', 1)
                did = 0
                while did < times:
                    did += 1
                    call = SKYPE.PlaceCall('lolahed.mm')
                    time.sleep(2)
                    call.Finish()
                status = call.Status
                send(status)
            #BOTCRASH
            elif msg.startswith(pyConf.trigger + 'botc'):
                mesg = msg.replace(pyConf.trigger + 'botc ', '', 1)
                t = 500
                d = 0
                while t > d:
                    crash = send(mesg)
                    d = d + 1
                    crash.Body = ''
            #FILTERLIST
            elif msg == pyConf.trigger+'filterlist':
                send(self.nick+'List: '+' - '.join(pyConf.filterList))
            #LOGTOFILE
            elif msg.startswith(pyConf.trigger + 'logtofile '):
                mesg = msg.replace(pyConf.trigger + 'logtofile ', '', 1)
                stats = send(self.nick + 'Setting logtofile mode to ' + mesg + '...')
                try:
                    pyFunctions.setConfigs(senderHandle,mesg,4)
                    if mesg == 'true':
                        stats.Body = (
                        self.nick + 'Logtofile set to ' + mesg + '\n Logs will now be saved to the Logs file.')
                    else:
                        stats.Body = (
                        self.nick + 'Logtofile set to ' + mesg + '\n Logs will now not be saved to the Logs file anymore.')
                except:
                    stats.Body = (self.nick + 'Failed setting logtofile mode.')
            #SAVEKEYS
            elif msg == pyConf.trigger + 'savekeys':
                keys = send(self.nick + 'Saving keys to different files...')
                pyFunctions.saveKeys()
                keys.Body = self.nick + 'Keys saved to ' + pyConf.dirKeys + '!'
            #GENKEYS
            elif msg.startswith(pyConf.trigger + 'genkeys '):
                qt = msg.replace(pyConf.trigger + 'genkeys ', '', 1)
                qt = int(qt)
                did = 0
                gener = send('Generating keys...')
                while qt > 0:
                    pyConf.premiumKeys.append(pyFunctions.genKeys(8, hash=True))
                    keys = open(pyConf.fileKeys, 'w')
                    keys.write('\n'.join(pyConf.premiumKeys))
                    qt -= 1
                    did += 1
                    gener.Body = (self.nick + 'Generated ' + str(did) + ' keys!')
            #STOP
            elif msg.startswith(pyConf.trigger + 'stop'):
                perc = 0
                string = 'Progress: [ . . . . . . . . . . . . . . . . . . . .] Closing threads...'
                stat = send(self.nick+string)
                done = False
                times = 0
                while done == False:
                    time.sleep(0.5)
                    times+=0.5
                    if times == 4:
                        string = string.replace('Closing threads...','Saving files...',1)
                    elif times == 8:
                        string = string.replace('Saving files...','Almost done...',1)
                    elif times == 10:
                        SKYPE.CurrentUserProfile._SetRichMoodText(self.nick+'PyBot is offline:(')
                        string = pyConf.name+' is shutting down(wave)'
                        done = True
                    string = string.replace(' .','#',1)
                    stat.Body = (self.nick+string)
                pyFunctions.stop()
            # RELOAD
            elif msg.startswith(pyConf.trigger + 'reload'):
                send(self.nick + 'Bot will reload in a few seconds!')
                SKYPE.CurrentUserProfile._SetRichMoodText(self.nick+'PyBot is reloading!')
                pyFunctions.restart()
            #ADDBOTS
            elif msg.startswith(pyConf.trigger+'addbots '):
                qnt = msg.replace(pyConf.trigger+'addbots ','',1)
                qnt = int(qnt)
                did = 0
                abots = send('Adding "'+str(qnt)+'" bots...')
                while did <= qnt:
                    send('/add pybot.'+pyFunctions.genKeys(3, hash=False,chara = True))
                    did +=1
                abots.Body = (self.nick+'Added "'+(str(qnt)+'" bots!:)'))
            #AUTOKICK
            elif msg.startswith(pyConf.trigger+'autokick '):
                mesg = msg.replace(pyConf.trigger+'autokick ','',1)
                autokick = send('Changing autokick mode...')
                pyFunctions.setConfigs(senderHandle,mesg,7)
                autokick.Body = (self.nick+'Changed autokick mode to '+mesg)
            #FILTERMODE
            elif msg.startswith(pyConf.trigger+'filtermode '):
                mesg = msg.replace(pyConf.trigger+'filtermode ','',1)
                filter = send('Changing filter mode...')
                try:
                 pyFunctions.setConfigs(senderHandle,mesg,6)
                except:
                    filter.Body = (self.nick+'Failed changing filter mode.')
                filter.Body = (self.nick+'Changed filter mode to '+mesg)
            #KICKBOTS
            elif msg == (pyConf.trigger + 'kickbots'):
                kicked = 0
                kbots = send(self.nick+'Kicking bots...')
                if pyPe.getAdmin(chat) == True:
                    status = False
                    chatMembers = chat.MemberObjects
                    for name in chatMembers:
                        if 'bot' in name.Handle:
                            status = True
                        else:
                            pass
                    if status == True:
                        for name in chatMembers:
                            if name.Handle != senderHandle and 'bot' in name.Handle:
                                send('/kick ' + name.Handle)
                                kbots.Body = (self.nick+'Kicked '+name.Handle+'.')
                                kicked +=1
                            else:
                                pass
                else:
                    pyDebug.error(pyConf.trigger + 'kickbots requires chat admin.')
                kbots.Body = (self.nick+'Kicked '+str(kicked)+' bots.')
        else:
            pass
