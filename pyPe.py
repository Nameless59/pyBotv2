import pyDebug
import pyFunctions
import Skype4Py


def getAdmin(chat):
    if chat.MyRole == 'MASTER' or chat.MyRole == 'CREATOR':
        return True
    else:
        return False


def getAPI():
    if pyFunctions.getOS() == True:
        return Skype4Py.Skype()
    else:
        return Skype4Py.Skype(Transport='x11')


def getHandle():
    return getAPI().CurrentUser.Handle


def versionCheck():
    currentVersion = getAPI().Version
    split = currentVersion.split('.')
    if int(split[0]) >= 6:
        if int(split[0]) > 6:
            pyDebug.errorExit('Downgrade Skype ' + split[0] + '.' + split[1] + ' to version 6.3 or below.')
        elif int(split[0]) == 6:
            if int(split[1]) < 4:
                pass
            else:
                pyDebug.errorExit('Downgrade Skype ' + split[0] + '.' + split[1] + ' to version 6.3 or below.')
        else:
            pass
    else:
        pass
